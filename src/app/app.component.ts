import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as X2JS from 'x2js';
import { UploadEvent, UploadFile, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { groupBy } from 'rxjs/internal/operators/groupBy';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('downloadLink') downloadLink: ElementRef;

  public files: UploadFile[] = [];
  title = 'BPDoc';
  loading = false;

  public metaData: {
    name: string,
    author: string
  }

  public bprelease: {
    groups: {
      id: string,
      name: string,
      members: string[]
    }[],
    objects: any[]
  }

  private groups;

  public templates: {
    id: string,
    name: string,
    group: string[],
    description: string,
    runmode: string,
    language: string,
    globalCode: string,
    code: string,
    references: string[],
    functions: {
      id: string,
      name: string,
      public: boolean,
      description: string,
      precondition: string[],
      postcondition: string[],
      inputs: {
        type: string,
        name: string,
        description: string,
      }[],
      outputs: {
        type: string,
        name: string,
        description: string,
      }[],
      dependencies: {
        name: string,
        type: string
      }[]
    }[]
  }[] =[];

  ngOnInit() {
  }

  public fileOver(event){
    console.log(event);
  }
 
  public fileLeave(event){
    console.log(event);
  }

  public dropped(event: UploadEvent) {
    this.files = event.files;
    for (const droppedFile of event.files) {
 
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
 
          // Here you can access the real file
          console.log(droppedFile.relativePath, file);
          
          this.onChangeInput(file);
 
          /**
          // You could upload it like this:
          const formData = new FormData()
          formData.append('logo', file, relativePath)
 
          // Headers
          const headers = new HttpHeaders({
            'security-token': 'mytoken'
          })
 
          this.http.post('https://mybackend.com/api/upload/sanitize-and-save-logo', formData, { headers: headers, responseType: 'blob' })
          .subscribe(data => {
            // Sanitized logo returned from backend
          })
          **/
 
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }


  onChangeInput(file) {
    this.loading = true;
    this.fileToText(file).then(text => {
      // this.readText = text;
      const x2js = new X2JS();
      const releaseFile = x2js.xml2js(text.replace(/&#x0;/g, ''));

      this.metaData = {
        name: releaseFile['release']['package-name'].__text,
        author: releaseFile['release']['user-created-by'].__text
      };

      this.groups = this.buildGroup(releaseFile['release']['contents']['object-group']);

      const objects = releaseFile['release'].contents.object.map(item => {
        const belongGroups = this.groups.filter(f => f.members.find(fMem => fMem === item._id) ).map(i => i.id);
        const processInfo = item.process.stage.find(f => f._type === 'ProcessInfo' );
        const stages = item.process.stage.filter(subItem => {
          return (subItem._type === 'Start' || subItem._type === 'End' || subItem._type === 'SubSheetInfo') && subItem.subsheetid;
        }).map(subItem => {
          if (subItem._type === 'Start') {
            let preconditions = [];
            let postconditions = [];
            let inputs = [];

            if (subItem.preconditions) {
              if (subItem.preconditions.condition) {
                if (subItem.preconditions.condition.map) {
                  preconditions = subItem.preconditions.condition.map(condItem => {
                    return condItem._narrative;
                  })
                } else {
                  preconditions = [subItem.preconditions.condition._narrative];
                }
              } 
            }

            if (subItem.postconditions) {
              if (subItem.postconditions.condition) {
                if (subItem.postconditions.condition.map) {
                  postconditions = subItem.postconditions.condition.map(condItem => {
                    return condItem._narrative;
                  })
                } else {
                  postconditions = [subItem.postconditions.condition._narrative];
                }

              }
            }

            if (subItem.inputs) {
              if (subItem.inputs.input) {
                if (subItem.inputs.input.map) {
                  inputs = subItem.inputs.input.map(inputItem => {
                    return {
                      type: inputItem._type,
                      name: inputItem._name,
                      description: inputItem._narrative,
                    }
                  })
                } else {
                  inputs = [
                    {
                      type: subItem.inputs.input._type,
                      name: subItem.inputs.input._name,
                      description: subItem.inputs.input._narrative,
                    }
                  ]
                }  
              }
            }
            return {
              sheetId: subItem.subsheetid,
              type: subItem._type,
              preconditions: preconditions,
              postconditions: postconditions,
              inputs: inputs
            }
          } else if (subItem._type === 'End') {
            let outputs = [];
            if (subItem.outputs) {
              if (subItem.outputs.output) {
                if (subItem.outputs.output.map) {
                  outputs = subItem.outputs.output.map(outputItem => {
                    return {
                      type: outputItem._type,
                      name: outputItem._name,
                      description: outputItem._narrative,
                    }
                  })
                } else {
                  outputs = [
                    {
                      type: subItem.outputs.output._type,
                      name: subItem.outputs.output._name,
                      description: subItem.outputs.output._narrative,
                    }
                  ]
                }
              }
            }
            return {
              sheetId: subItem.subsheetid,
              type: subItem._type,
              outputs: outputs
            }
          } else {
            return {
              sheetId: subItem.subsheetid,
              type: subItem._type,
              description: subItem.narrative
            }
          }
          
        });

        // subsheetの内容を集約
        const functions = item.process.subsheet.filter(subItem => {
          return subItem.name !== 'Clean Up';
        }).map(subItem => {
          const inputStage = stages.find(f => f.sheetId === subItem._subsheetid && f.type === 'Start');
          const outputStage = stages.find(f => f.sheetId === subItem._subsheetid && f.type === 'End');
          const infoStage = stages.find(f => f.sheetId === subItem._subsheetid && f.type === 'SubSheetInfo');
          const dependencies = [];

          return {
            id: subItem._subsheetid,
            name: subItem.name,
            description: infoStage.description,
            public: Boolean(subItem.published),
            precondition: inputStage.preconditions,
            postcondition: inputStage.postconditions,
            inputs: inputStage.inputs,
            outputs: outputStage.outputs,
            dependencies: dependencies
          };
        });

        return { 
          id: item._id,
          name: item._name,
          group: belongGroups,
          description: item.process._narrative,
          runmode: item.process._runmode,
          language: processInfo.language,
          globalCode: processInfo.globalcode,
          code: processInfo.code,
          references: processInfo.references.reference,
          functions: functions
        };
      });

      this.templates = objects;
      this.bprelease = {
        groups: this.groups,
        objects: objects
      }
      this.download(JSON.stringify(this.bprelease), 'text/json', file.name.replace('bprelease', 'json'));

    }).catch(err => console.log(err));

  }

  fileToText(file): any {
    const reader = new FileReader();
    reader.readAsText(file);
    return new Promise((resolve, reject) => {
      reader.onload = () => {
        resolve(reader.result);
      };
      reader.onerror = () => {
        reject(reader.error);
      };
    });
    
  }

  private buildGroup(group: any[]): {
    id: string,
    name: string,
    members: string[]
  }[] {
    const groups = group.map(item => {
      let members: any[] = [];
      const objList = item['members']['object'];
      if (objList['_id']) {
        members.push(objList['_id']);
      } else {
        members = objList.map(obj => obj['_id']);
      }

      return {
        id: item['_id'],
        name: item['_name'],
        members: members
      };
    })
    return groups;
  }

  private download(data: string, type: string, filename: string) {
    const bom = '\uFEFF';
    const blob = new Blob([data], { type: type });
    const anchor: any = document.createElement('a');

    if (window.navigator.msSaveBlob) {
      window.navigator.msSaveBlob(blob, filename);
  
      // chrome, firefox, etc.
    } else if (window.URL && anchor.download !== undefined) {
      anchor.download = filename;
      anchor.href = window.URL.createObjectURL(blob);
      document.body.appendChild(anchor);
      anchor.click();
      anchor.parentNode.removeChild(anchor);
      
    } else {
      window.location.href =
      'data:attachment/csv;charset=utf-8,' + encodeURIComponent(data);
      
    }
  }

  private templateHtml = '';

}
